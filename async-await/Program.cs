﻿var rand = new Random();
var randNum = rand.Next(0, 10);
int[] array = new int[10];

int[] result = await createArrayAsync();
result = await multiplyArrayAsync(result, randNum);
result = await sortArrayAsync(result);
await averageArrayAsync(result);
async Task<int[]> createArrayAsync()
{
    Console.WriteLine("Generated array:");
    for (int i = 0; i < 10; i++)
    {
        array[i] = rand.Next(0, 100);
        Console.WriteLine(array[i]);
    }
    return array;
}

async Task<int[]> multiplyArrayAsync(int[] result, int randNum)
{
    Console.WriteLine("Multiplied array by " + randNum + ":");
    for (int i = 0; i < 10; i++)
    {
        result[i] *= randNum;
        Console.WriteLine(result[i]);
    }
    return result;
}

async Task<int[]> sortArrayAsync(int[] result)
{
    result = result.OrderBy(x => x).ToArray();
    Console.WriteLine("Sorted array:");
    for (int i = 0; i < 10; i++)
    {
        Console.WriteLine(result[i]);
    }
    return result;
}

async Task averageArrayAsync(int[] result)
{
    int sum = 0;
    for (int i = 0; i < 10; i++)
    {
        sum += result[i];
    }
    Console.WriteLine("Mean value is " + sum / 10);
}